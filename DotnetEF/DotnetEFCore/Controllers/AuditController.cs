﻿using DotnetEFCore.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace DotnetEFCore.Controllers
{
    public class AuditController : Controller
    {
        private readonly ApplicationDbContext _context;
        public AuditController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<AuditEntity> audits = _context.AuditEntities.ToList();
            return View(audits);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("PICName, AssetId, AntiVirus, OfficeLicense, WindowsLicense, AssetCondition")] AuditEntity request)
        {
            _context.AuditEntities.Add(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            var entity = _context.AuditEntities.Find(id);
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, PICName, AssetId, AntiVirus, OfficeLicense, WindowsLicense, AssetCondition")] AuditEntity request)
        {
            _context.AuditEntities.Update(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }
        [HttpGet]
        public IActionResult Delete(long? id)
        {
            var entity = _context.AuditEntities.Find(id);
            if (entity == null)
            {
                return Redirect("/Audit/GetAll");
            }
            _context.AuditEntities.Remove(entity);
            _context.SaveChanges();
            return Redirect("/Audit/GetAll");
        }
    }
}
