﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEFCore.DataContext
{
    [Table("audit_table")]
    public class AuditEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("pic_name")]
        public string PICName { get; set; }

        [Column("asset_id")]
        public long AssetId { get; set; }

        [Column("antivirus")]
        public bool AntiVirus { get; set; }

        [Column("office_license")]
        public bool OfficeLicense { get; set; }

        [Column("windows_license")]
        public bool WindowsLicense { get; set; }

        [Column("asset_condition")]
        public string AssetCondition { get; set; }

        public AuditEntity() { }

    }
}
