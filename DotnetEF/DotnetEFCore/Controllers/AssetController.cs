﻿using DotnetEFCore.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace DotnetEFCore.Controllers
{
    public class AssetController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AssetController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<AssetEntity> assets = _context.AssetEntities.ToList();
            return View(assets);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Name, PurchaseYear, SerialNumber, Specification")] AssetEntity request)
        {
            // add to entity
            _context.AssetEntities.Add(request);
            // commit to database
            _context.SaveChanges();

            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            var entity = _context.AssetEntities.Find(id);
            return View(entity);
        }
        [HttpPost]
        public IActionResult Update([Bind("Id, Name, PurchaseYear, SerialNumber, Specification")] AssetEntity request)
        {
            // update entity
            _context.AssetEntities.Update(request);
            // commit to database
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            var assetEntity = _context.AssetEntities.Find(id);
            if (assetEntity == null)
            {
                return Redirect("/Asset/GetAll");
            }
            // remove from entity
            _context.AssetEntities.Remove(assetEntity);
            // commit to database
            _context.SaveChanges();
            return Redirect("/Asset/GetAll");
        }
    }
}
