﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DotnetEFCore.DataContext
{
    [Table("return_asset_table")]
    public class ReturnAssetEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("asset_id")]
        public long AssetId { get; set; }

        [Column("pic_name")]
        public string PICName { get; set; }

        [Column("return_date")]
        public DateTime ReturnDate { get; set; }

        public ReturnAssetEntity() { }

    }
}
