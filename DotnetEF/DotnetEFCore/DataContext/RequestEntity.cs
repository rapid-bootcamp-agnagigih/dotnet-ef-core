﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DotnetEFCore.DataContext
{
    [Table("request_table")]
    public class RequestEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("asset_id")]
        public long AssetId { get; set; }

        [Column("pic_name")]
        public string PICName { get; set; }

        [Column("specification_req")]
        public string SpecificationReq { get; set; }


        public RequestEntity() { }
    }
}
