﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEFCore.DataContext
{
    [Table("asset_table")]
    public class AssetEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("purchase_year")]
        public int PurchaseYear { get; set; }

        [Column("serial_number")]
        public string SerialNumber { get; set; }

        [Column("specification")]
        public string Specification { get; set; }

        public AssetEntity() { }

    }
}
