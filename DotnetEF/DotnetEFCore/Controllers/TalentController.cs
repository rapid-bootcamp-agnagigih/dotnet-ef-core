﻿using DotnetEFCore.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace DotnetEFCore.Controllers
{
    public class TalentController : Controller
    {
        private readonly ApplicationDbContext _context;
        public TalentController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<TalentEntity> talents = _context.TalentEntities.ToList();
            return View(talents);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Name, Email, Education, PlaceOfBirth, DateOfBirth")] TalentEntity request)
        {
            _context.TalentEntities.Add(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            var entity = _context.TalentEntities.Find(id);
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, Name, Email, Education, PlaceOfBirth, DateOfBirth")] TalentEntity request)
        {
            _context.TalentEntities.Update(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            var entity = _context.TalentEntities.Find(id);
            if (entity == null)
            {
                return Redirect("/Talent/GetAll");
            }
            _context.TalentEntities.Remove(entity);
            _context.SaveChanges();
            return Redirect("/Talent/GetAll");
        }
    }
}
