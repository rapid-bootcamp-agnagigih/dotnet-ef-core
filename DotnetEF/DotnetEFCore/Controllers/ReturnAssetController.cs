﻿using DotnetEFCore.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace DotnetEFCore.Controllers
{
    public class ReturnAssetController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReturnAssetController(ApplicationDbContext context) { _context = context; }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<ReturnAssetEntity> returnAssets = _context.ReturnAssetEntities.ToList();
            return View(returnAssets);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("AssetId, PICName, ReturnDate")] ReturnAssetEntity request)
        {
            request.ReturnDate = DateTime.Now;
            _context.ReturnAssetEntities.Add(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            var entity = _context.ReturnAssetEntities.Find(id);
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, AssetId, PICName, ReturnDate")] ReturnAssetEntity request)
        {
            _context.ReturnAssetEntities.Update(request);
            _context.SaveChanges();
            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            var entity = _context.ReturnAssetEntities.Find(id);
            if (entity == null)
            {
                return Redirect("/Request/GetAll");
            }
            _context.ReturnAssetEntities.Remove(entity);
            _context.SaveChanges();
            return Redirect("/Request/GetAll");
        }
    }
}
