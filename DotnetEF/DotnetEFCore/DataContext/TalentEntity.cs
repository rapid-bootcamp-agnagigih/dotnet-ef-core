﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DotnetEFCore.DataContext
{
    [Table("talent_table")]
    public class TalentEntity
    {
        [Key]
        [Column("id")]
        public long Id { get; set; }
        
        [Column("name")]
        public string Name { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("education")]
        public string Education { get; set; }

        [Column("place_of_birth")]
        public string PlaceOfBirth { get; set; }

        [Column("date_of_birth")]
        public DateTime DateOfBirth { get; set; }

        public TalentEntity() { }

    }
}
