﻿using DotnetEFCore.DataContext;
using Microsoft.EntityFrameworkCore;

namespace DotnetEFCore.DataContext
{
    public class ApplicationDbContext : DbContext
    {
        // injection
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        public DbSet<AssetEntity> AssetEntities => Set<AssetEntity>();
        public DbSet<AuditEntity> AuditEntities => Set<AuditEntity>();
        public DbSet<RequestEntity> RequestEntities => Set<RequestEntity>();
        public DbSet<ReturnAssetEntity> ReturnAssetEntities => Set<ReturnAssetEntity>();
        public DbSet<TalentEntity> TalentEntities => Set<TalentEntity>();

    }

}
